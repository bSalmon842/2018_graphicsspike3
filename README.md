---
Spike Report
---

Graphics Spike 3
================

Introduction
------------

This Spike was completed in 2017 and the folder structure and repo was
updated in 2018.

Unreal Engine contains a very powerful lighting engine, being able to
manipulate this can make simulations and game scenes look much better.

Bitbucket Link: <https://bitbucket.org/bSalmon842/graphicsspike3>

Goals
-----

-   A small indoor scene, with light coming in from the windows, and a
    three-point lighting setup for a high-poly "feature" model placed
    upon a table in the middle of the room (get one online).

    -   Start using the [Lighting Quick Start
        Guide](https://docs.unrealengine.com/latest/INT/Engine/Rendering/LightingAndShadows/QuickStart/index.html)

    -   Have most objects Static

    -   Have the feature object Movable (so the player can knock it
        around by walking into it).

    -   Use at least one Point Light and one Spot Light

    -   Have at least one smooth and/or metallic object, and use a
        Reflection Capture (box or sphere) to aid in reflection.

    -   Use a [Light
        Profile](https://docs.unrealengine.com/latest/INT/Engine/Rendering/LightingAndShadows/IESLightProfiles/index.html)
        on as many lights as plausible.

    -   For the windows, create two copies of the level.

        -   One will use
            [light-cards](https://www.unrealengine.com/blog/simulating-area-lights-in-ue4)

        -   One will use [Lightmass
            portals](https://docs.unrealengine.com/latest/INT/Support/Builds/ReleaseNotes/2016/4_11/index.html#new:lightmassportals)

Personnel
---------

  ------------------------- ------------------
  Primary -- Brock Salmon   Secondary -- N/A
  ------------------------- ------------------

Technologies, Tools, and Resources used
---------------------------------------

-   <https://docs.unrealengine.com/latest/INT/Engine/Rendering/LightingAndShadows/>

-   <https://www.turbosquid.com/3d-models/free-ceramic-wine-jug-3d-model/1126837>

Tasks undertaken
----------------

-   To setup the basic environment a room was made using the Quickstart
    Guide. Atmospheric Fog, a Directional Light and a Lightmass
    Importance Mass.

-   The Starter Content Table was then placed with a Ceramic Jug that
    was downloaded (which can be knocked around by the fly-cam) and the
    Unreal Engine example object on top

-   For the 3-point lighting, the key light was a point light setup in
    between the table and the door

-   The Fill light was a spot light placed in the corner adjacent to the
    Door and the Back light was a point light set up in the corner
    opposite the door

-   A Box Reflection capture was then placed around the Example object

What we found out
-----------------

From this Spike we learned how to use multiple types of volumes to
create and optimise effective lighting, as well as how to create scenes
using the different available light types. From this spike we also got a
handle of different ways to change the appearance of an Unreal Engine scene
using Fog, Lights and Importance Masses. This Spike also demonstrates how
to create a scene with 3-point lighting.